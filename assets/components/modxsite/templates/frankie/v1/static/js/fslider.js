// v. 0.0.1-alpha

function fslider(element,options){
    if(!options.sliderClass) options.sliderClass = '.fslider';
    var cls = {
        slider:             options.sliderClass,
        wrapper:            options.sliderClass +'__wrapper',
        box:                options.sliderClass +'__box',
        current:            options.sliderClass +'__box--current',
        arrows:             options.sliderClass +'__arrow-box',
        arLeft:             options.sliderClass +'__arrows--prev',
        arRight:            options.sliderClass +'__arrows--next',
    };

    var findBox = element.find('>*');
    element.append('<div class="'+ cls.wrapper.replace('.','') +'"></div>');
    findBox.each(function(key){
        element.find(cls.wrapper).append('<div class="'+cls.box.replace('.','')+'"></div>');
        element.find(cls.box).eq(key).append($(this));
    });

    var elem = {
        fslider:            element,
        fwrapper:           element.find(cls.wrapper),
        fbox:               element.find(cls.box),
        farrows:            element.find(cls.arrows),
        farPrev:            element.find(cls.arLeft),
        farNext:            element.find(cls.arRight),
    };

    var defaultOptions = options;
    if (!defaultOptions.nextCountSlide) {
        defaultOptions.nextCountSlide = defaultOptions.countSlide;
    }
    if (!defaultOptions.prevCountSlide) {
        defaultOptions.prevCountSlide = defaultOptions.countSlide;
    }

    var _ = $.extend({}, defaultOptions);

    $(cls.slider).append("<div class='fslider__arrow-box'><div class='fslider__arrows fslider__arrows--prev'>"+_.arRightCont+"</div><div class='fslider__arrows fslider__arrows--next'>"+_.arLeftCont+"</div></div>");

    var _fsWidth = {},
        _fsGo = true,
        _fsliderBoxCount = elem.fbox.length,
        _fsLeft = 0,
        _fsAnimateTime = 300,
        _isDraggable = false,
        _drag,
        _minDragRange = 5;

    // function setIndex(){
    //     var index = 0;
    //     if(_.center){
    //         index = Math.floor((- _fsLeft + _fsWidth.offset) / _fsWidth.box);
    //     } else {
    //         index = Math.floor(Math.abs(_fsLeft) / _fsWidth.box);
    //     }

    //     elem.fslider.find(cls.current).removeClass(cls.current.replace('.',''));
    //     elem.fslider.find('[data-fs-index='+ index +']').addClass(cls.current.replace('.',''));
    // }


    function slide(direction, count){
        if (_fsGo && !_isDraggable) {
            var len = _fsWidth.box * count,
                left = _fsLeft;

            switch(direction){
                case "right":
                    left += len;
                    break;
                case "left":
                    left -= len;
                    break;
            }
            _fsGo = false;

            setLeft(left);

            setTimeout(function(){
                _fsGo = true; 
                // setIndex();
            },_fsAnimateTime);
        }
    }

    function goToSlide(index,animate) {
        if(typeof animate == 'undefined') animate = false;
        if(!elem.fslider.find('[data-fs-index='+ index +']').is(cls.current)){
            var goTo = parseFloat(_fsWidth.box * index);

            if(_.center) goTo -= _fsWidth.offset;

            if(animate){
                setWrapperAnimate(true);
            }
            setLeft(-goTo, animate);
            
            // setTimeout(function(){
            //    setIndex();
            // },_fsAnimateTime);
        }
    }

    function initSizes(){

        _fsWidth.slider = parseFloat(elem.fslider.width());
        _fsWidth.box = _fsWidth.slider / _.slideNum;
        _fsWidth.wrapper = _fsliderBoxCount * _fsWidth.box;
        _fsWidth.offset = Math.floor(_.slideNum / 2) * _fsWidth.box;

        $.each( _.response, function(key, value){
            if (document.body.clientWidth <= key) {
                $.extend(_, value);
                return false;
            } else if (document.body.clientWidth > key) {
                $.extend(_, defaultOptions);
            }
        });

        if(!_.arrows) {
            $(cls.arrows).hide();
        } else {
            $(cls.arrows).show();
        }
        if(_fsliderBoxCount <= _.slideNum){
            $(cls.arrows).hide();
        }

        setSizes();
    }

    function setWrapperAnimate(animate){
        if(animate){
            elem.fwrapper.css({
                transition: _fsAnimateTime +'ms'
            });
        } else {
            elem.fwrapper.css({
                transition: 'none'
            });
        }
    }

    function setLeft(left,animate,draggable){
        if(typeof animate == 'undefined') animate = true;
        if(typeof draggable == 'undefined') draggable = false;
        var leftEdge = 0,
            rightEdge = _fsWidth.slider - _fsWidth.wrapper,
            isEdge = false;

        if(_.center) {
            leftEdge += _fsWidth.offset;
            rightEdge -= _fsWidth.offset;
        }
        if(left > leftEdge){
            if(!draggable){
                left = leftEdge;
            }
            isEdge = true;
        }
        if(left < rightEdge){
            if(!draggable){
                left = rightEdge;
            }
            isEdge = true;
        }
        _fsLeft = left;
        
        if(!animate){
            setWrapperAnimate(false);
        }
        elem.fwrapper.css({
            'transform': 'translate3d('+_fsLeft+'px,0,0)'
        });
        return isEdge;
    }

    function setSizes(){
        elem.fbox.css({ 
            width: _fsWidth.box
        });
        elem.fwrapper.css({
            width: _fsWidth.wrapper
        });
        if(_.center) {
            if(_fsLeft >= 0){
                setLeft(_fsWidth.offset, false);
            }
        } else {
            _fsWidth.offset = 0;
            setLeft(_fsWidth.offset, false);
        }
    }
    
    function time(){
        return parseInt(new Date().getTime());
    }
    
    function dragStart(e){
        var x0 = 0;
        
        _drag = {
            time: time(),
            range: 0,
            isEdge: false
        };
        
        if(typeof _drag.inertiaTimer != 'undefined'){
            clearInterval(_drag.inertiaTimer);
        }
        
        $(document).on('touchmove mousemove', function(e){
            var x1 = 0;
            if(e.originalEvent.touches){
                x1 = e.originalEvent.touches[0].clientX;
            }else{
                x1 = e.clientX;
            }
            
            if(x0 !== 0){
                if(!_isDraggable) {
                    _isDraggable = true;
                }
                _fsLeft += x1 - x0;
                _drag.range += x1 - x0;
            }
            x0 = x1;
            
            _drag.isEdge = setLeft(_fsLeft + x1 - x0, false, true);
        });
    }

    function dragEnd(e){
        $(document).off('touchmove mousemove');
        
        // вычисляем инерцию движения
        if(_.inertia){
            if(_drag.range && !isNaN(_drag.range) && !isNaN(_drag.time)){
                _drag.time = (time() - _drag.time) / 1000;
                var m = 0.05, // масса
                    k = 0.8, // коэффициент затухания
                    // a = 5, // ускорение
                    v = _drag.range / _drag.time, // скорость
                    // s = v * _drag.time + (a * _drag.time * _drag.time) / 2, // расстояние
                    p = v * m; // импульс
                    
                var drag = _drag;
                var timer = setInterval(function(){
                    _fsLeft += p;
                    p *= k;
                    
                    setLeft(_fsLeft, false);
                    if(drag.isEdge || Math.abs(p) <= 1){
                        clearInterval(timer);
                        _fsLeft = Math.round(_fsLeft / _fsWidth.box) * _fsWidth.box;
                        
                        setWrapperAnimate(true);
                        setLeft(_fsLeft);
                    }
                },16);
                _drag.inertiaTimer = timer;
            }
        } else {
            setLeft(Math.round(_fsLeft / _fsWidth.box) * _fsWidth.box, true);
        }

        setTimeout(function(){
            _isDraggable = false;
        },_fsAnimateTime);
        
        _drag = {};
    }

    initSizes();
    initSizes();
    
    setWrapperAnimate(true);

    if(_.startSlide){
        goToSlide(_.startSlide,false);
    }
    elem.fbox.each(function(i){
        $(this).attr('data-fs-index',i);
    });
    // setIndex();

    // events
    elem.fslider
        .on('click', cls.arRight, function(){ 
            slide("left" ,_.nextCountSlide);
    })
        .on('click', cls.arLeft, function(){ 
            slide("right" ,_.prevCountSlide);
    })
        .on('click', cls.box, function(e) {
            if(!_isDraggable)
                goToSlide(parseInt($(this).attr('data-fs-index')),true);
    });

    elem.fwrapper
      .on('mousedown',function(e){
        dragStart(e);
        e.preventDefault();
        return false;
    })
      .on('touchstart',function(e){
        dragStart(e);
    });
    $(document)
      .on('touchend touchcancel',function(e){
        dragEnd(e);
    })
      .on('mouseup mouseleave',function(e){
        dragEnd(e);
    });

    $(window).on('resize', function() {
        initSizes();
    });
    
    return {
        goToSlide: goToSlide
    };
}

$.fn.fslider = function(options){
	var _defaultOptions = $.extend({
            slideNum:           1,
            countSlide:         1,
            center:             false,
            inertia:            true,
            arrows:             true,
            arRightCont:        "сюда",
            arLeftCont:         "туда",
            response : {
                480 : {
                    slideNum:       1,
                }
            }
    	}, options ? options : {}),
    	$this = this;
    
    return $this.each(function(){
        this.fs = new fslider($(this),_defaultOptions);
    });
};
