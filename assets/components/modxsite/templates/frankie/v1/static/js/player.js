
(function ($) {

    $.fn.fsPlayer = function (playlist, userOptions) {
    var jPlayerDefaults = {
            swfPath: "../dependencies/jquery-jplayer",
            supplied: "mp3",
            cssSelectorAncestor: '.jp-interface',
            errorAlerts: false,
            warningAlerts: false
        };

        return this.each(function($playlist, $options){
      var $jPlayer = $('.jPlayer-container'),
        $fs = $(this),
        $fsLayout = $('#player-layout');
        $currentTrackIndex = 0,
        $currentTrack = playlist[$currentTrackIndex],
        $playing = false,
        $types = [{
            alias: 'repeat',
            icon: '&#xE040;'
        },{
            alias: 'repeat-one',
            icon: '&#xE041;'
        },{
            alias: 'shuffle',
            icon: '&#xE043;'
        }],
        $type = 0,
        $isFirst = true;

      var $playTimer;

      jPlayerOptions = $.extend({}, jPlayerDefaults);

      function rand( min, max, exclude ) {
        var rand = min;
          if( max ) {
              rand = Math.floor(Math.random() * (max - min + 1)) + min;
          } else {
            max = min;
              rand = Math.floor(Math.random() * (max + 1));
          }
          if(typeof exclude !== 'undefined' && exclude == rand){
            rand--;
            if(rand < 0) {
              rand = max;
            }
          }
          return rand;
      }


      function init(){
        $('.jp-pause').hide();

        $jPlayer.bind($.jPlayer.event.ready, function () {

                    function setVolume(volume) {
                        var volumes = [0, 33, 66, 100],
                            volumeClasses = ['mute', 'low', 'medium', 'high'],
                            volumeClass = volumeClasses[0];

                        var vlp = volumes[0];
                        $.each(volumes, function(i, vln){
                            if(i > 0) {
                                if(volume >= vlp && volume <= vln){
                                    volumeClass = volumeClasses[i];
                                    vlp = vln;
                                }
                            }
                        });
                        if(volume == 0)
                            volumeClass = volumeClasses[0];

                        $jPlayer.jPlayer("volume", volume / 100);
                        $fs.find('.jp-volume').attr('data-level', volumeClass);
                    }
                    
                    function repeatModeAction(){
                
              if($type != 0 && $type != 2){
                  $fs.find('.jp-next,.jp-prev').prop('disabled',false);
                if($currentTrackIndex == playlist.length - 1){
                    $fs.find('.jp-next').prop('disabled',true);
                } else if($currentTrackIndex == 0){
                    $fs.find('.jp-prev').prop('disabled',true);
                }
            }
                    }

          function play(){
            setTrackInfo();

            togglePlayClasses('play');
            updateTime(true);
        //    repeatModeAction();
            $('.slider').slick('slickGoTo', $currentTrackIndex);

            $jPlayer.jPlayer('play');
          }

          function playTrackToIndex(index){
            if(typeof index !== 'undefined' && $currentTrackIndex != index){
              $currentTrackIndex = index;
                $jPlayer.jPlayer("setMedia", playlist[index]);
            }
            play();
          }

          function togglePlayClasses(type){           
            var trackInList = $fsLayout.find('#list-large [data-track-index='+ $currentTrackIndex +']');
            var trackInSlider = $fsLayout.find('#main-slider [data-track-index='+ $currentTrackIndex +']');

            if(type == 'play'){
              $fs.find('.jp-play').hide();
              $fs.find('.jp-pause').show();

              $fsLayout.find('.jp-play').show();
              $fsLayout.find('.jp-pause').hide();
              trackInSlider.find('.jp-play').hide();
              trackInSlider.find('.jp-pause').show();
              $fsLayout.find('.slider__play-box--play').removeClass('slider__play-box--play');
              trackInSlider.find('.slider__play-box').addClass('slider__play-box--play');

              $fsLayout.find('.jp-play-in-list.active').removeClass('active');
              $fsLayout.find('.jp-play-in-list.active--play').removeClass('active--play');
              $fsLayout.find('.jp-current-time-in-list').each(function(){
                  var t = $(this);
                  if(t.text() != t.attr('data-default-time')) {
                      t.text(t.attr('data-default-time'));
                  }
              });
              trackInList.addClass('active').addClass('active--play');
            } else {
              $fs.find('.jp-pause').hide();
              $fs.find('.jp-play').show();

              trackInSlider.find('.jp-play').show();
              trackInSlider.find('.jp-pause').hide();

              trackInList.removeClass('active--play');
            }
          }

          function pause(){
            var trackInList = $fsLayout.find('[data-track-index='+ $currentTrackIndex +']');

            togglePlayClasses('pause');
            updateTime(false);

            $jPlayer.jPlayer('pause');
          }

          function next(){
              var index = $currentTrackIndex;
            if($type == 2){
              index = rand(0, playlist.length - 1, index);
            } else {
              index++;
              if(index >= playlist.length) {
                index = 0;
              }
            }
            playTrackToIndex(index);
          }

          function prev(){
              var index = $currentTrackIndex;
            if($type == 2){
              index = rand(0, playlist.length - 1, index);
            } else {
              index--;
              if(index < 0) {
                index = playlist.length - 1;
              }
            }         
            playTrackToIndex(index);
          }
          
          function stop(){
            $jPlayer.jPlayer('stop');
          }

          function setVolume(volume) {
                        var volumes = [0, 33, 66, 100],
                            volumeClasses = ['mute', 'low', 'medium', 'high'],
                            volumeClass = volumeClasses[0];

                        var vlp = volumes[0];
                        $.each(volumes, function(i, vln){
                            if(i > 0) {
                                if(volume >= vlp && volume <= vln){
                                    volumeClass = volumeClasses[i];
                                    vlp = vln;
                                }
                            }
                        });
                        if(volume == 0)
                            volumeClass = volumeClasses[0];

                        $jPlayer.jPlayer("volume", volume / 100);
                        $fs.find('.jp-volume').attr('data-level', volumeClass);
                    }

                    function setTrackInfo(){
                      $currentTrack = playlist[$currentTrackIndex];

                      if(
                        $currentTrackIndex != $fsLayout.find('.jp-play-in-list.active').attr('data-track-index')
                        || $isFirst
                      ){
                        var animateTime = 200,
                          originalCoverBg = $('.jp-controls-cover-image'),
                          cloneCoverBg = originalCoverBg.clone().addClass('is-cloned');
              $('body').prepend(cloneCoverBg);

              $('.jp-controls-cover > img,.jp-controls-cover-image.is-cloned').attr('src', $currentTrack.cover);

              originalCoverBg.css({"opacity":1}).animate({"opacity":0},animateTime);
              cloneCoverBg.css({"opacity":0}).animate({"opacity":1},animateTime);

              setTimeout(function(){
                originalCoverBg.remove();
                cloneCoverBg.removeClass('is-cloned')
              },animateTime);
            }

            $('.jp-controls-info-title').text($currentTrack.title);
            $('.jp-controls-info-artist').text($currentTrack.artist);
                    }

                    function setTimeText(){
                      $fsLayout.find('.jp-play-in-list.active .jp-current-time-in-list').text($fs.find('.jp-current-time').text());
                    }

                    function updateTime(flag){
                      clearInterval($playTimer);
                      if(flag){
                        setTimeText();
                        $playTimer = setInterval(function(){                            
                          setTimeText();
                        },1000);
                      }
                    }


                  $jPlayer.jPlayer("setMedia", playlist[$currentTrackIndex]);
                  setTrackInfo();
                  $isFirst = false;

                    //Bind jPlayer events
                    $jPlayer.bind($.jPlayer.event.ended, function () {
                        switch($type){
                        case 1:
              play();
                            break;
                        default:
                          next();
                        }
            return false;
                    });

                    $jPlayer.bind($.jPlayer.event.play, function () {
                        $jPlayer.jPlayer("pauseOthers");
                    });

                    $jPlayer.bind($.jPlayer.event.playing, function () {
                        $playing = true;
                    });

                    $jPlayer.bind($.jPlayer.event.pause, function () {
                        $playing = false;
                    });


          // bind control buttons
          $('body').on('click','.jp-play',function(){
            if($(this).closest('[data-track-index]').length){
              playTrackToIndex(parseInt($(this).closest('[data-track-index]').attr('data-track-index')));
            } else {
              play();
            }
            return false;
          });

          $('body').on('click','.jp-pause',function(){
            pause();
            return false;
          });

          $fs.on('click','.jp-next',function(){
            next();
            return false;
          });

          $fs.on('click','.jp-previous',function(){
            prev();
            return false;
          });

          $fs.on('click','.jp-volume',function(){
                        var vlmSlider = $fs.find('.jp-volume-change'),
                            prevVolume = vlmSlider.attr('data-value') > 0 ? vlmSlider.attr('data-value') : 100,
                            volume = 0;

                        if($(this).attr('data-level') == 'mute'){
                            vlmSlider.val(prevVolume);
                            volume = prevVolume;
                        } else {
                            vlmSlider.val('0');
                            volume = 0;
                        }
                        setVolume(volume);

            return false;
          });

                    $fs.on('input','.jp-volume-change',function(e) {
                        var volume = $(this).val();
                        setVolume(volume);
                        $(this).attr('data-value',volume);
                    });

                    $fs.on('click','.jp-type-switch',function(){
                        $type++;
                        if($type >= $types.length){
                            $type = 0;
                        }
                        $(this).find('> i').html($types[$type].icon);
        //    repeatModeAction();
                    });

          $fs.on('click','.jp-toggle-layout',function(){
            var t = $(this);
            if(t.is('.player-controls__button--active')){
              t.removeClass('player-controls__button--active');
              $(t.attr('data-alternate')).hide();
              $(t.attr('data-href')).show();
            } else {              
              t.addClass('player-controls__button--active');
              $(t.attr('data-alternate')).show();
              $(t.attr('data-href')).hide();
            }
            $('.slider').slick('slickGoTo', $currentTrackIndex, true); //.slick('refresh',true)
          });

          $fs.on('mouseup','.jp-seek-bar',function(){
            setTimeout(function(){
              setTimeText();
            },50);
          });

          $fsLayout.on('click','.jp-play-in-list',function(){
            var t = $(this),
              trackIndex = parseInt(t.attr('data-track-index'));

            if(t.is('.active--play')){
              pause();
            } else {
              playTrackToIndex(trackIndex);
            }
          });
                });

        $jPlayer.jPlayer(jPlayerOptions);
      }

      init();
    });
    };

})(jQuery);
