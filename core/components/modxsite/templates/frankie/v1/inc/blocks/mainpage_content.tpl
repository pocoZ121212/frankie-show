
    {$params = [
        'where' => [
            'template' => 3
        ]
        ,'limit' => 0
        ,'sort' => 'menuindex'
        ,'dir' => 'asc'
    ]}
    {if $modx->resource->template == 3}
        {$params.where.id = $modx->resource->id}
    {else}
        {$params.where.parent = $modx->resource->id}
        {$params.depth = 5}
    {/if}
    {if $modx->resource->id == $modx->getOption('site_start')}
        {$params.limit = 5}
        {$params.sort = 'RAND()'}
    {/if}
    {processor action="web/playlist/getdata" ns=modxsite params=$params assign=result}
    
    {*<pre>{$result|print_r}</pre>*}

    {$playlist = []}
    {foreach $result.object as $object}
        {$playlist[] = $object.info}
    {/foreach}
    
    <div id="player-playlist" style="display:none">{json_encode($playlist)}</div>
    
    <div id="main-slider" class="mdl-tabs__panel is-active">
        <div class="slider fslider">
            
            {$i = 0}
            {foreach $result.object as $object}
                
                {include file="inc/blocks/track_item.tpl"}
                
            {$i = $i + 1}
            {/foreach}
            
        </div><!-- END .slider -->
    </div><!-- END #main-slider -->

    <div id="list-large" class="mdl-tabs__panel">
        <div class="list-large">
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--6-col list-large__cell-image">
                    <div class="jp-controls-cover list-large__box">
                        <img src="{$result.0.cover}" alt="">
                    </div>
                </div>
                <div class="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet list-large__cell-playlist">
                    <div class="list-large__box">
                        <div class="list-large__overflow">
                                
                            <div class="list-large__list-item">
                                <span class="list-large__album">Альбом</span>
                                <span class="list-large__track-title"><em>Название трека</em></span>
                            </div>
                            <ul class="list-large__list">
                                
                                {$i = 0}
                                {foreach $result.object as $object}
                                    {$track = $object.info}
                                
                                    <li class="list-large__list-item list-large__list-item--counter jp-play-in-list{if $i==0} active{/if}" data-track-index="{$i}">
                                        <span class="list-large__album">{$track.artist}</span>
                                        <span class="list-large__track-title">{$track.title}</span>
                                        <span class="jp-current-time-in-list list-large__time" data-default-time="{$track.duration}">{$track.duration}</span>
                                    </li>
                                    
                                {$i = $i + 1}
                                {/foreach}
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END .list-large -->
    </div><!-- END #list-large -->