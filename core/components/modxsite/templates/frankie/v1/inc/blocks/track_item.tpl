{$track = $object.info}

<div class="slider__box" data-track-index="{$i}" itemscope itemtype="http://schema.org/AudioObject">
    {$dur = explode(':',$track.duration)}
    <meta itemprop="duration" content="PT{$dur.0}M{$dur.1}S">
    <meta itemprop="isFamilyFriendly" content="true">
    <meta itemprop="encodingFormat" content="mp3">
    <meta itemprop="image" content="{$modx->getOption('site_url')}{$track.cover}">
    <meta itemprop="contentUrl" content="{$modx->getOption('site_url')}{$track.mp3}">
    <meta itemprop="uploadDate" content="{date('Y-m-d\TH:i:s',$object.createdon)}">
    
    <div class="slider__image-box">
        <span>
            <img src="{snippet name=phpthumbof params=[
                'input' => $track.cover
                ,'options' => 'w=590&h=590&zc=1'
            ]}" alt="">
        </span>
        <div class="slider__play-box">
            <button class='jp-play mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored'>
                <i class='material-icons'>&#xE037;</i>
            </button>
            <button class="jp-pause mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="display:none;">
                <i class="material-icons">&#xE034;</i>
            </button>
        </div>
        {* ссылка на скачивание (с аяксом не работает)
        <div class="slider__info-box">
            <button id="demo-menu-lower-right-{$object.id}"
                    class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons">more_vert</i>
            </button>
            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                for="demo-menu-lower-right-{$object.id}">
                <li class="mdl-menu__item">
                    <a href="assets/components/modxsite/connectors/connector.php?pub_action=download&id={$object.id}" download> 
                        Скачать "{array_pop(explode('/',$track.mp3))}"
                    </a>
                </li>
            </ul>
        </div>
        *}
    </div>
    <div class="slider__desc">
        <p class="slider__title" itemprop="name">
            <a href="{$object.uri}" title="{$track.title}">{$track.title}</a>
        </p>
        <p class="slider__artist">{$track.artist}</p>
        <p itemprop="author" itemscope itemtype="http://schema.org/Person" class="hidden">
            <span itemprop="name">Френки Сумасшедший</span>
            {*<meta itemprop="image" content="">*}
        </p>
        {if $object.description}
            <p itemprop="description" class="hidden">{$object.description}</p>
        {/if}
    </div>
</div>