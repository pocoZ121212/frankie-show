<!DOCTYPE html>
<html lang="ru">
    <head>
        <!-- git init -->
        <base href="{$modx->getOption('site_url')}">
        {snippet name=addMeta}
        
        <title>{$modx->resource->pagetitle} | Фрэнки-шоу</title>
        <meta name="yandex-verification" content="66e399b62f58b4a4" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="assets/images/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/images/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        {literal}
        <style>
            body{overflow:hidden}
            @-moz-keyframes rotate{0%{-moz-transform:rotate(0);transform:rotate(0)}
                100%{-moz-transform:rotate(360deg);transform:rotate(360deg)}
            }
            @-webkit-keyframes rotate{0%{-webkit-transform:rotate(0);transform:rotate(0)}
                100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}
            }
            @keyframes rotate{0%{-moz-transform:rotate(0);-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0)}
                100%{-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-webkit-transform:rotate(360deg);transform:rotate(360deg)}
            }
            .js-mask{background:#888;position:fixed;top:0;bottom:0;left:0;right:0;z-index:10}
            .js-loader{content:"";display:inline-block;position:absolute;width:50px;height:50px;top:50%;margin-top:-25px;left:50%;margin-left:-25px;z-index:1;border-radius:50%;border:3px solid #3F51B5;border-left-color:transparent;background:0 0;animation:rotate .4s infinite linear}
        </style>
        {/literal}
    </head>

    <body>
        
    {block name=sub_body}{/block}
        
    <div class="js-mask">
        <div class="js-loader"></div>
    </div>
    
    <img src="" class="jp-controls-cover-image background-filter-image" alt="">

    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <header class="mdl-layout__header mdl-layout__header--transparent">
        <div class="mdl-layout__header-row">
          <div class="mdl-layout-spacer"></div>
          {*<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
            <label class="mdl-button mdl-js-button mdl-button--icon"
                   for="fixed-header-drawer-exp">
              <i class="material-icons">&#xE8B6;</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" name="sample"
                     id="fixed-header-drawer-exp">
            </div>
          </div>*}
          <div class="">
            <div class="nav__info">
                <button id="show-dialog" type="button" class="mdl-button mdl-js-button mdl-button--icon">
                  <i class="material-icons">&#xE88F;</i>
                </button>
            </div>
          </div>
        </div>
      </header>

      <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Медиатека</span>
            {snippet name='wayfinder' params=[
                'startId' => 1, 
                'level' => 1, 
                'hereTpl' => '@CODE <a class="mdl-navigation__link mdl-navigation__link--current" href="[[+wf.link]]">[[+wf.linktext]]</a>', 
                'rowTpl' => '@CODE <a class="mdl-navigation__link" href="[[+wf.link]]">[[+wf.linktext]]</a>',
                'outerTpl' => '@CODE <nav class="mdl-navigation">[[+wf.wrapper]]</nav>'
            ]}
      </div>
    </div>
    <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
        <div id="player-layout">
            {block name=content}
            
            {processor action="site/web/resources/getdata" ns=modxsite params=[
                'where' => [
                    'parent' => 1
                ]
                ,'limit' => 0
                ,'sort' => 'menuindex'
                ,'dir' => 'asc'
            ] assign=result}
            
            {if $result.success && $result.total}
                <p> &nbsp; </p>
                <p> &nbsp; </p>
                <p> &nbsp; </p>
                <nav class="mdl-navigation">
                    {foreach $result.object as $object}
                        <a href="{$object.uri}" class="mdl-button">{$object.pagetitle}</a>
                    {/foreach}
                </nav>
            {/if}
            
            {/block}
        </div>

        <div id="player-interface" style="display:none;">
            <div class="player-controls">
                <div class="jPlayer-container"></div>
                <div class="player-controls__player jp-interface">
                    <div class="player-controls__time-box">
                        <div class="player-controls__time jp-current-time"></div>
                        <div class="player-controls__progress-wrapper">
                            <div class="player-controls__progress-bar jp-seek-bar">
                                <div class="player-controls__elaps-bar jp-play-bar"></div>
                            </div>
                        </div>
                        <div class="player-controls__duration jp-duration"></div>
                    </div>
                    <div class="jp-controls player-controls__left-box">
                        <button class="jp-previous player-controls__button mdl-button mdl-js-button mdl-js-ripple-effect">
                            <i class="fa fa-fast-backward" aria-hidden="true"></i>
                        </button>

                        <button class="jp-play player-controls__button player-controls__button--fsize-big player-controls__button--play mdl-button mdl-js-button mdl-js-ripple-effect">
                            <i class="fa fa-play" aria-hidden="true"></i>
                        </button>

                        <button class="jp-pause player-controls__button player-controls__button--fsize-big mdl-button mdl-js-button mdl-js-ripple-effect">
                            <i class="fa fa-pause" aria-hidden="true"></i>
                        </button>

                        <button class="jp-next player-controls__button mdl-button mdl-js-button mdl-js-ripple-effect">
                            <i class="fa fa-fast-forward" aria-hidden="true"></i>
                        </button>

                        <!-- Переключение в плейлист -->
                        <div data-href="#main-slider" data-alternate="#list-large" class="jp-toggle-layout toggle-layout player-controls__button player-controls__button--material player-controls__list-large mdl-button mdl-js-button">
                            <i class="material-icons">&#xE05F;</i>
                        </div>
                        <!-- END Переключение в плейлист -->

                    </div>

                    <div class="jp-controls-cover player-controls__album-cover">
                        <img src="" alt="" />
                    </div>
        
                    <div class="jp-controls-info player-controls__track-info">
                        <div class="jp-controls-info-title player-controls__title"></div>
                        <div class="jp-controls-info-artist player-controls__artist"></div>
                    </div>
        
                    <div class="player-controls__right-box">
                        <div class="jp-type-switch player-controls__button player-controls__button--material-min mdl-button mdl-js-button mdl-js-ripple-effect">
                            <i class="material-icons">&#xE040;</i>
                        </div>
                        <div class="jp-volume volume player-controls__button mdl-button mdl-js-button mdl-js-ripple-effect" data-level="high"></div>
                        <div class="player-controls__volume-slider">

                            <input class="jp-volume-change mdl-slider mdl-js-slider" type="range" min="0" max="100" value="100" data-value="100" tabindex="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END .mdl-tabs -->
    
        <link href="{$template_url}static/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=cyrillic,cyrillic-ext" rel="stylesheet">
        
        <!-- <link rel="stylesheet" href="https://code.getmdl.io/1.2.0/material.indigo-deep_purple.min.css" />  -->
        <link href="{$template_url}static/css/material.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{$template_url}static/css/style.css">
    
        <script src="{$template_url}static/js/jquery.js"></script>

        <script src="{$template_url}static/js/material.min.js"></script>
        <script src="{$template_url}static/js/jquery.jplayer.min.js"></script>
        {*<script src="{$template_url}static/js/jquery.touchSwipe.min.js"></script>*}
        <script src="{$template_url}static/js/fslider.js"></script>
        <script src="{$template_url}static/js/player-2.0.js"></script>
        <script>
            var _fsOptions = {
                slideNum: 3,
                center: true,
                countSlide: 1,
                arrows: true,
                arRightCont: "<button class='mdl-button mdl-js-button mdl-button--icon slider__prev'><i class='material-icons'>&#xE408;</i></button>",
                arLeftCont: "<button class='mdl-button mdl-js-button mdl-button--icon slider__next'><i class='material-icons'>&#xE409;</i></button>",
                response : {
                    768 : {
                        slideNum: 1,
                        center: false,
                        arrows: false,
                    },
                }
            };
            $('.fslider').fslider(_fsOptions);
            var _fslider = $('.fslider')[0].fs;
        </script>
        
        <script type="text/javascript">
            var myPlaylist = $.parseJSON($('#player-playlist').text());
            
            var _fsp = new fsPlayer(myPlaylist);
            _fsp.init();
        </script>
        
        <script>
        function _toAjax(data,url){
            $('.js-mask').fadeIn();
            $.ajax({
                url: '{$modx->getOption('site_url')}/' + url,
                data: data,
                error: function(e){
                    $('.js-mask').fadeOut();
                    console.log(e);
                },
                success: function(response){
                    var html = $(response).find('#player-layout');
                    $('#player-layout').html(html.html());
                    
                    $('.fslider').fslider(_fsOptions);
                    _fslider = $('.fslider')[0].fs;
                    
                    _fsp.setPlaylist($.parseJSON(html.find('#player-playlist').text()));
                    
                    if(url != window.location){
                        window.history.pushState(null, null, url);
                    }
                    
                    $('.js-mask').fadeOut();
                }
            });
        }
        
        $(function(){
            
            $('body').on('click','.mdl-navigation a,.slider__title a',function(e){
                if($(this).hasClass('mdl-navigation__link'))
                    $('.mdl-layout__obfuscator').click();
                
                $(this).parent().find('.mdl-navigation__link--current').removeClass('mdl-navigation__link--current');
                $(this).addClass('mdl-navigation__link--current');
                _toAjax({},$(this).attr('href'));
                e.preventDefault();
            });
            
            // slickInit();
        });
            
        </script>
        
        <script src="{$template_url}static/js/app.js"></script>
        
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.4/dialog-polyfill.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.4/dialog-polyfill.min.js"></script>
          <dialog class="mdl-dialog">
            <h5 class="mdl-dialog__title">Фан-сайт радиопередачи &laquo;Фрэнки Шоу&raquo;</h5>
            <div class="mdl-dialog__content">
              <p>
                Связь с администрацией сайта:
              </p>
              <a class="mdl-navigation__link mdl-navigation__link--current" href="mailto:frankie.show@ya.ru">frankie.show@ya.ru</a>
            </div>
            <div class="mdl-dialog__actions">
              <button type="button" class="mdl-button close">Закрыть</button>
            </div>
          </dialog>
          <script>
            var dialog = document.querySelector('dialog');
            var showDialogButton = document.querySelector('#show-dialog');
            if (! dialog.showModal) {
              dialogPolyfill.registerDialog(dialog);
            }
            showDialogButton.addEventListener('click', function() {
              dialog.showModal();
            });
            dialog.querySelector('.close').addEventListener('click', function() {
              dialog.close();
            });
          </script>
        
        <script>$('.js-mask').fadeOut()</script>
        
        {if $modx->getOption('site_url') != 'http://dev.frankie-show.ru/'}
            {include file="inc/metrika/yandex.tpl"}
        {/if}
    </body>
</html>
