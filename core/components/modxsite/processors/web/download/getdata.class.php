<?php
class downloadProcessor extends modProcessor{
    
    private function translit($str,$sep='-',$options=array()) {
        if($sep===false) 
            $sep = '';
        if(!$options['case']) {
            $options['case'] = 'lower';
        }
        $str = strtr($str, array(
            "ый"=>"y","ЫЙ"=>"Y",
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"zh","з"=>"z","и"=>"i","й"=>"i","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"shch","ь"=>"","ы"=>"y","ъ"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"Zh","З"=>"Z","И"=>"I","Й"=>"I","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"o","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"Ts","Ч"=>"Ch","Ш"=>"Sh","Щ"=>"Shch","Ь"=>"","Ы"=>"Y","Ъ"=>"","Э"=>"E","Ю"=>"Yu","Я"=>"Ya",
        ));
        $exclude = 'a-zA-Z0-9';
        if($options['exclude']){
            if(is_array($options['exclude'])){
                $options['exclude'] = implode('',$options['exclude']);
            }
            $exclude .= $options['exclude'];
        }
        $out = trim(preg_replace(array('/[^'.$exclude.']/is','/'.$sep.'{2,}/is'),$sep,$str),$sep);
        switch ($options['case']) {
            case 'upper': $out = mb_strtoupper($out); break;    
            case 'lower': $out = mb_strtolower($out); break;
            default:
        }
        return $out;
    }
    
    public function process(){
        
        if($id = $this->getProperty('id')){
            $source = $this->modx->getObject('sources.modMediaSource', 8)->toArray();
            $source = $source['properties']['basePath']['value'];
            
            $url = $this->modx->getObject('modTemplateVarResource',array('contentid' => $id, 'tmplvarid' => 1))->value;
            
            $file = array();
            
            $file['path'] = $source . ltrim($url,'/');
            $file['name'] = $this->translit(array_pop(explode('/',$url)),'_',array('exclude' => '\.'));
            $file['size'] = filesize($file['path']);
            
            // print '<pre>'; print_r($file); exit;
            
            $response = $this->output($file);
        } else {
            return $this->failure('Не указан путь до файла');
        }
        
        return $response;
    }
    
    public function output(array $file = array()){
        $response = array(
            'success' => 0,
            'message' => 'Нет данных'
        );
        
		if (file_exists($file['path'])) {
            
            // читаем файл и отправляем его пользователю
            if ($fd = fopen($file['path'], 'rb')) {
                
                // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
                // если этого не сделать файл будет читаться в память полностью!
                if (ob_get_level()) {
                    ob_end_clean();
                }
                
                // заставляем браузер показать окно сохранения файла
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . $file['name']);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . $file['size']);
                
                while(!feof($fd)) {
                    print fread($fd, 1024);
                }
                fclose($fd);
                exit;
            } else {
		        $response = $this->failure('Доступ к файлу запрещен!');
            }
		} else {
		    $response = $this->failure('Файл не существует!');
		}
		
		return $response;
    }
}
return 'downloadProcessor';