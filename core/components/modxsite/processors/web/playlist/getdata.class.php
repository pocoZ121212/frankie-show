<?php

require_once dirname(dirname(__FILE__)) . '/resources/getdata.class.php';

include_once(__DIR__ . '/get_mp3_info.php');
        
class modWebPlaylistGetdataProcessor extends modWebResourcesGetdataProcessor {
    
    public function initialize(){
        return parent::initialize();
    }
    
    public function afterIteration(array $list){
        $list = parent::afterIteration($list);
        
        foreach($list as &$l){
            $tmp = array(
                'mp3' => $this->setImageSource($l['tvs']['track']['value'], 8)
                // ,'duration' => !empty($l['tvs']['duration']['value']) ? $l['tvs']['duration']['value'] : '--:--'
                ,'cover' => $this->setImageSource($l['tvs']['image']['value'], 4)
                ,'title' => $l['pagetitle']
                ,'artist' => $this->modx->getObject('modResource',$l['parent'])->pagetitle
            );
            
            $info = getMP3data($this->modx->getOption('base_path') . $tmp['mp3']);
            $info['id3v1']['name'] = iconv('windows-1251','utf-8',$info['id3v1']['name']);
            $info['id3v1']['artists'] = iconv('windows-1251','utf-8',$info['id3v1']['artists']);
            $tmp['duration'] = $info['duration_str'] ? $info['duration_str'] : '--:--';
            
            $l['info'] = $tmp;
        }
        
        return $list;
    }
    
}

return 'modWebPlaylistGetdataProcessor';