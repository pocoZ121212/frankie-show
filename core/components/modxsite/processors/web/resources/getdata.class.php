<?php

require_once dirname(dirname(dirname(__FILE__))).'/site/web/resources/getdata.class.php';

class modWebResourcesGetdataProcessor extends modSiteWebResourcesGetdataProcessor {

public function initialize(){
        if(!$page = $this->getProperty('page')) {
            $this->setProperty('page',$this->modx->getOption('page',$_REQUEST,1));
        }
        return parent::initialize();
    }
    
    
    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $c = parent::prepareQueryBeforeCount($c);
        
        // доп параметр - depth: глубина вложенности
        if($depth = $this->getProperty('depth')){                
            $where = $this->getProperty('where');
            $parents = explode(',',$where['parent']);
            unset($where['parent']);
            $where['parent:IN'] = $parents;
            
            // 
            foreach($parents as $par){
                $where['parent:IN'] = array_merge($where['parent:IN'],$this->modx->getChildIds($par));            
            }
            
            $this->setProperty('where', $where);
            $c->where($where);
        }
        
        //  в параметре tvFilter через запятую укажем по каким тв будем фильтровать
        if($filter = $this->getProperty('tvFilter')){
            $alias = $c->getAlias();
            // выделим все поля из основной таблицы, чтобы не фильтровать по ним как по тв
            $all_fields = $this->modx->newObject($alias);
            
            if(!is_array($filter)) {
                $filter = explode(',',$filter);
            }
            
            foreach($filter as $key){
                
                if(!isset($all_fields->{$key}) || !$all_fields->{$key}){
                    
                    if(is_numeric($key)){
                        $tv = $this->modx->getObject('modTemplateVar', $key);
                    } else {
                        $tv = $this->modx->getObject('modTemplateVar', array('name' => $key));
                    }
                    // найдем тв и приджойним его
                    if($tv && !empty($where["{$tv->name}.value"])){
                        $c->innerJoin('modTemplateVarResource',  "{$tv->name}", "{$alias}.id = {$tv->name}.contentid AND {$tv->name}.tmplvarid = {$tv->id}");
                        $c->select(array("{$tv->name}.value"));
                        $c->select();
                    }
                }
            }
        }
        
        // $c->prepare();
        // echo $c->toSQL();
        // exit;
        
        if($where = $this->getProperty('where')){
            $c->where($where);
        }
        
        return $c;
    }
    
    public function setImageSource($image_path,$image_source=''){
        if(substr($image_path,0,1)!='/' && $image_source){
            $image_path = $this->getSourcePath($image_source) . $image_path;
        }
        return ltrim($image_path,'/');
    }
    
}


return 'modWebResourcesGetdataProcessor';